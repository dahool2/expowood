<?php
require_once __DIR__ . '/vendor/autoload.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class CreatePdf
{
    private $today;
    private $mpdf;
    private $headerImgPath;
    // private $table;

    public $name;
    public $phone;
    public $clientType;
    public $legalName;
    public $regNo;
    public $bags;
    public $price;
    public $priceWVat;
    public $vat;
    public $priceForItem;

    public function __construct()
    {
        $this->today = date('Y.m.d');
        $this->mpdf = new \Mpdf\Mpdf(['tempDir'=>__DIR__.'/pdfTmp','debug'=>true]);
        $this ->headerImgPath = __DIR__.'/images/expowood.png';
    }


    public function getDate(){
        return $this->today;
    }

    public function buildPdf(){

        $num=0;
        $num = $this->getBillNumber();
        $str='';
        if($this->clientType!='phs'){
            $str= 'Reg. nr';
        }
        
        $body =<<<EOF
<br/>
<table style="width:100%;margin-top:25px">
<tbody>
<tr>
<td style="text-align:center;font-size:18px">Rēķins Nr: $num</td>
</tr>
<tr>
<td>&nbsp;SIA EXPOWOOD</td>
</tr>
<tr>
<td>&nbsp;Sila iela 1-21, Babīte, Babītes nov., LV2101, Latvija</td>
</tr>
</tbody>
</table>

<br/>
<div style="width:100%; text-align:center;color:red;">$this->today</div>
<br/>

<table style="width: 100%; border-top:2px solid black; border-bottom:2px solid black;">
    <tbody>
        <tr>
            <td>Piegādātājs</td>
            <td><strong>EXPOWOOD,SIA</strong></td>
            <td>PVN Nr</td>
            <td>LV40003382868</td>
        </tr>
        <tr>
            <td>Jur. adrese</td>
            <td>Sila iela 1 - 21, Babīte, LV-2101, Latvia</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Banka</td>
            <td>AS "SWEDBANK"</td>
            <td>Kods</td>
            <td>HABALV22</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td>Konts</td>
            <td>LV76HABA0001408051826</td>
        </tr>
    </tbody>
</table>
<table style="width:100%; border-bottom:2px solid black">
    <tbody>
        <tr>
            <td>Saņēmējs</td>
            <td align="left">$this->legalName  $this->name</td>
            <td>Tālrunis</td>
            <td align="left">$this->phone</td>
        </tr>
        <tr>
            <td colspan=2>
            <td>$str</td>
            <td align="left">$this->regNo</td>
        </tr>
    </tbody>
</table>
<br/><br/>
EOF;

$table='<table style="width: 100%; border-collapse: collapse; ">
<tbody>
    <tr style="border: 1px solid black; text-align: center; font-weight: bold;">
        <th style="border: 1px solid black;">Nosaukums</th>
        <th style="border: 1px solid black;">Daudzums</th>
        <th style="border: 1px solid black;">Cena (EUR)</th>
        <th>Summa (Eur)</th>
    </tr>
    <tr style="border: 1px solid black; border-bottom:3px solid black;">
        <td style="border: 1px solid black;">Kokskaidu granulas, 6mm</td>
        <td style="border: 1px solid black;" align="center">'.$this->bags.'</td>
        <td style="border: 1px solid black;" align="center">'.round($this->priceForItem,2) .'</td>
        <td style="border: 1px solid black;" align="center">'.round($this->priceWVat,2).'</td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td style="border: 1px solid black;">Summa (EUR)</td>
        <td style="border: 1px solid black;" align="center">'.round($this->priceWVat,2).'</td>
    </tr>
    <tr >
        <td colspan="2"></td>
        <td style="border: 1px solid black;">PVN % (EUR)</td>
        <td style="border: 1px solid black;" align="center">'.round($this->vat,2).'</td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td style="border: 1px solid black;">Summa apmaksai (EUR)</td>
        <td style="border: 1px solid black;" align="center">'.round($this->price,2).'</td>
    </tr>
</tbody>
</table>';

$htmlHeader='<div style="width:100%; text-align:center;"><img src="'.$this->headerImgPath.'" style="height:25px"/></div>';
$this->mpdf ->SetHTMLHeader($htmlHeader);
$this->mpdf->WriteHTML($body.$table);

$this->mpdf->Output(__DIR__.'/bills/bill'.$num.'.pdf','F');
return $num;
//$this->mpdf->Output();
    }

    private function getBillNumber()
    {
        $f = fopen(__DIR__.'/bills/order.txt','r');
        $num = fgets($f);
        fclose($f);

        $num ++;
        $f = fopen(__DIR__.'/bills/order.txt','w');
        fwrite($f,$num);
        fclose($f);
        return $num;
    }


}
?>


