<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';
require 'pdf.php';



if (strlen(gP('g-recaptcha-response') > 0)) {

    $pdf = new CreatePdf();
    
    $priceForPadWVat = 165.9;
    $priceForPad = $priceForPadWVat * 1.12;

    $totalPads = (gP('pallets'));
    $price = 0;
    $priceWVat=0;
    $vat = 0;
    $str="";
    $client = gP('inlineRadioOptions');

    
    $priceWVat = $totalPads * $priceForPadWVat;
    $pdf->priceForItem = $priceForPadWVat;
    

    if ($client == 'phs') {
        $pdf->priceWVat=$priceWVat;
        $price = $priceWVat * 1.12;
        $vat = $price - $priceWVat;
        $pdf->price =$price;
        $pdf->clientType = 'phs';
    }
    else{
        $vat=0.00;
        $price = $priceWVat;
        $pdf->price =$priceWVat;
        $pdf->priceWVat=$priceWVat;
        $pdf->clientType='lgl';
    }
    
    $pdf->clientType = gP('inlineRadioOptions');
    $pdf->name = gP('PhysName');
    $pdf->phone = gP('Phone');
    $pdf->legalName = gP('LegalName');
    $pdf->regNo = gP('RegNumber');
    $pdf->bags = $totalPads;
    $pdf->vat = $vat;
    
    ob_clean();
    ob_get_clean();
    $num = $pdf->buildPdf();

    $mail = new PHPMailer(true);
    $mailUser = 'rekini@expowood.eu';
    $mailUserPass = 'tr&UoN%4';
    $messageSubject = 'Rēķins';
    $messageBodyHTML = "Jums ir sagatavots jauns rēķins. 
                   <br/>Jus pasūtījat:
                   <ul>
                    <li>Maisus: " . gP('bags') . "</li>
                    <li>Palletes: " . gP('pallets') . "</li>
                   </ul>
                   <br/>
            Pasūtījuma summa: ".round($price,2)." eur (PVN ".round($vat,2)." EUR)";
    $messageBodyALT = "Jums ir sagatavots jauns rēķins. ";
    $from = "rekini@expowood.eu";

    try {
        //Server settings
        $mail->CharSet = 'UTF-8';
        $mail->SMTPDebug = SMTP::DEBUG_OFF;                      // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP
        $mail->Host       = 'mail.expowood.eu';                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = $mailUser;                              // SMTP username
        $mail->Password   = $mailUserPass;                          // SMTP password
        $mail->SMTPSecure = 'ssl'; //PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
        $mail->Port       = 465;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($from, 'rekini');
        $mail->addAddress(gp('Email'), gp('Name').gP('LegalName'));     // Add a recipient
        $mail->addCC('pellets@expowood.eu');
        $mail->addCC('stanislavs.barkovskis@hotmail.com');

        // Attachments
        $mail->addAttachment(__DIR__.'/bills/bill'.$num.'.pdf');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $messageSubject;
        $mail->Body    = $messageBodyHTML;
        $mail->AltBody = $messageBodyALT;

        $mail->send();
        echo '<script type="text/javascript">window.location.href="/?order=ok"</script>';
    } catch (Exception $e) {
        //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        echo '<script type="text/javascript">window.location.href="/?order=error"</script>';
    }
    //echo '<pre>';
}


function gP($key)
{
    if (!isset($_POST[$key])) {
        return null;
    } else {
        return $_POST[$key];
    }
}


?>