<?php

?>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	<link rel="stylesheet" href="css/main.css">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<title>ORDER_FORM</title>
</head>

<body class="order-body">
	<div class="container order-form-bg">
		<div class="row">
			<div class="col">
				<div class="order-container">
					<div class="form-group">
						<label for="bags">Мешки</label>
						<input type="number" min=0 class="form-control" id="bags" placeholder="Количество мешков" />
					</div>
					<div class="form-group">
						<label for="pads">Паллеты</label>
						<select class="form-control" id="pads" placeholder="Паллеты">
							<option value="0">Паллеты</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
						</select>
					</div>
					<div>
						<p>
							Сумма <span class="total-price">0 EUR</span>
						</p>
					</div>
					<div class="form-group">
						<button type="button" class="btn btn-primary" onclick="openNav()" id="Quantity">Заказать</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<form action="/processOrder.php" method="POST" name="order-form" id="order-form" class="needs-validation" novalidate>
		<div id="myNav" class="overlay-order">
			<div class="overlay-order-content">
				<div class="container">
					<div class="form-row">
						<div class="col">
							<h1>ORDER_FORM</h1>
						</div>
					</div>
					<div class="form-row">
						<div class="col text-left">
							<div class="form-check form-check-inline">
								<input class="form-check-input radio" type="radio" name="inlineRadioOptions" checked id="physical" value="phs">
								<label class="form-check-label" for="physical">Fiziska persona</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input radio" type="radio" name="inlineRadioOptions" id="legal" value="lgl">
								<label class="form-check-label" for="legal">Juridiska persona</label>
							</div>
						</div>
					</div>
					<br/>
					<div class="form-row">
						<div class="col">
							<div class="form-group phys">
								<label for="formNameSurname">Name,Surname</label>
								<input type="hidden" name="bags" id="Bags-post"/>
								<input type="hidden" name="pallets" id="Pallets-post"/>
								<input type="text" 
									class="form-control" 
									id="formNameSurname" 
									placeholder="Name, Surname"
									name="PhysName"
									required>
								
								<div class="invalid-feedback">Please provide name and surname.</div>
							</div>

							<div class="form-group legal d-none">
								<label for="formLegalName">Company name</label>
								<input type="text" 
									class="form-control" 
									id="formLegalName" 
									placeholder="Company name"
									name="LegalName"
									>
								
								<div class="invalid-feedback">Please provide company name.</div>
							</div>

							<div class="form-group legal d-none">
								<label for="formRegNumber">Reg. number</label>
								<input type="text" 
									class="form-control" 
									id="formRegNumber" 
									placeholder="Reg. number"
									name="RegNumber"
									>
								
								<div class="invalid-feedback">Please provide reg number.</div>
							</div>
							
							<div class="form-group">
								<label for="formPhone">Phone</label>
								<input type="tel" 
									class="form-control" 
									id="formPhone" 
									placeholder="Phone"
									name='Phone'
									required>
								<div class="invalid-feedback">Please provide phone number.</div>
							</div>
							<div class="form-group">
								<label for="formEmail">E-Mail</label>
								<input type="email" 
								class="form-control" 
								id="formEmail" 
								name="Email"
								placeholder="Email"
								required>
							<div class="invalid-feedback">Please provide email address.</div>
							</div>

							<div class="form-group">
								<strong>ORDER_PRICE: <span id="Price-preview"></span></strong>
							</div>
							<div class="form-group">
								<div class="g-recaptcha" data-sitekey="6Lc5S7wUAAAAAN0w6PtkpT0dwYUe08xsCjUcC3zo"></div>
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-info" />
								<a href="javascript:void(0)" onclick="closeNav()" class="btn btn-danger">Cancel</a>
							</div>
						</div>
					</div>
	</form>
	<script type="text/javascript">

(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
	var forms = document.getElementsByClassName('needs-validation');
	var jsonObj={};
	var radios = document.getElementsByClassName('radio');

	var r = Array.prototype.filter.call(radios,function(radio){
		radio.addEventListener('change',function(){
			clearForm();
			var lgls = document.querySelectorAll('.legal input');
			var phs = document.querySelectorAll('.phys input');

			

			if(radio.value=='lgl'){
				lgls.forEach((i)=>{
					i.parentNode.classList.remove('d-none');
					i.setAttribute('required','required');
					calculatePrice(totalBags,false);
				});

				phs.forEach((i)=>{
					i.parentNode.classList.add('d-none');
					i.removeAttribute('required');
				});

			}
			else{
				lgls.forEach((i)=>{
					i.parentNode.classList.add('d-none');
					i.removeAttribute('required');
				});

				phs.forEach((i)=>{
					i.parentNode.classList.remove('d-none');
					i.setAttribute('required','required');
					calculatePrice(totalBags,true);
				});
			}
		},false)
	},false);

    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
		}
		else{
			event.preventDefault();
          	event.stopPropagation();
			console.log('check');
			var captchaResponse = grecaptcha.getResponse();
			if(captchaResponse.length>0){
				form.submit();
			}
			
		}
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

		const bagInput = document.querySelector("#bags");
		const padInput = document.querySelector("#pads");
		const price = document.querySelector(".total-price");

		const postBags = document.querySelector("#Bags-post");
		const postPallets = document.querySelector("#Pallets-post");
		const pricePreview = document.querySelector('#Price-preview');

		const priceForBag = 2.61;
		const priceForPad = 2.40;
		const physVatRate = 12;
		const bagsInPallet = 70;

		var totalBags = 0;
		var bagInputFunction = function(event) {

			
			totalBags =0;
			quantityBags = Number(bagInput.value);
			if(quantityBags<0){
				bagInput.value="";
				return; 
			}
			
			if(quantityBags>=70)
			{
				bagInput.value = (quantityBags % 70).toFixed();
				padInput.value = Math.floor(quantityBags / 70);
				quantityBags = Number(bagInput.value);
			}


			let quantityPads = Number(padInput.value);

			if(quantityBags>0)
				totalBags+=quantityBags;

			
			if(quantityPads>0)
				totalBags=quantityBags +quantityPads * bagsInPallet;
			
			
			postBags.value = quantityBags;
			postPallets.value = quantityPads;

			  calculatePrice(totalBags,true);
		}



		bagInput.addEventListener('input', bagInputFunction, false);
		padInput.addEventListener('change', bagInputFunction, false);


		function calculatePrice(bags,phys) {
			let totalPrice=0;
			let vat = 0;
			console.log(bags);
			if(bags >=70 )
			{
				totalPrice = bags * priceForPad;
			}
			else{
				totalPrice = bags * priceForBag;
			}
			if(phys){
				let priceWithVat = totalPrice * 1.12;
				vat = (priceWithVat - totalPrice ).toFixed(2);
				price.innerText = priceWithVat.toFixed(2) + " EUR (" + vat + " EUR PVN)";
				pricePreview.innerText = priceWithVat.toFixed(2) + " EUR (" + vat + " EUR PVN)";
			}
			else{
				vat = 0.00;
				price.innerText = (totalPrice).toFixed(2) + " EUR (" + 0.00 + " EUR PVN)";
				pricePreview.innerText = (totalPrice ).toFixed(2) + " EUR (" + 0.00 + " EUR PVN)";
			}

		}

		function openNav() {
			document.getElementById("myNav").style.height = "100%";
		}

		function closeNav() {
			document.getElementById("myNav").style.height = "0%";
			clearForm();
			
		}

		function clearForm(){
			let form = document.getElementById('order-form').elements;
			for(i=0;i<form.length;i++){
				if(form[i].type=="text" || form[i].type=="tel" || form[i].type=="email"){
					form[i].value="";
				}
			}
		}
	</script>
</body>

</html>