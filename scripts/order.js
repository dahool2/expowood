(function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      var jsonObj={};
      var radios = document.getElementsByClassName('radio');
  
      var r = Array.prototype.filter.call(radios,function(radio){
          radio.addEventListener('change',function(){
              clearForm();
              var lgls = document.querySelectorAll('.legal input');
              var phs = document.querySelectorAll('.phys input');
  
              
  
              if(radio.value=='lgl'){
                  lgls.forEach((i)=>{
                      i.parentNode.classList.remove('d-none');
                      i.setAttribute('required','required');
                      calculatePrice(totalBags,false);
                  });
  
                  phs.forEach((i)=>{
                      i.parentNode.classList.add('d-none');
                      i.removeAttribute('required');
                  });
  
              }
              else{
                  lgls.forEach((i)=>{
                      i.parentNode.classList.add('d-none');
                      i.removeAttribute('required');
                  });
  
                  phs.forEach((i)=>{
                      i.parentNode.classList.remove('d-none');
                      i.setAttribute('required','required');
                      calculatePrice(totalBags,true);
                  });
              }
          },false)
      },false);
  
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          else{
              event.preventDefault();
                event.stopPropagation();
              console.log('check');
              var captchaResponse = grecaptcha.getResponse();
              
              if(captchaResponse.length>0){
                  form.submit();
              }
              
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
  
          
          const padInput = document.querySelector("#pads");
          const price = document.querySelector(".total-price");
  
          const postPallets = document.querySelector("#Pallets-post");
          const pricePreview = document.querySelector('#Price-preview');
  
          const priceForBag = 2.61;
          const priceForPad = 165.9;
          const physVatRate = 12;
          const bagsInPallet = 70;
  
          var totalBags = 0;
          var bagInputFunction = function(event) {

              totalBags =0;
              let quantityPads = Number(padInput.value);
  
              
              if(quantityPads>0)
                  totalBags=quantityPads;
              
              
              postPallets.value = quantityPads;
  
                calculatePrice(totalBags,false);
          }
  
  
  
           padInput.addEventListener('change', bagInputFunction, false);
  
  
          function calculatePrice(bags,phys) {
              let totalPrice=0;
              let vat = 0;
              
              totalPrice = bags * priceForPad;

              if(phys){
                  let priceWithVat = totalPrice * 1.12;
                  vat = (priceWithVat - totalPrice ).toFixed(2);
                  price.innerText = priceWithVat.toFixed(2) + " EUR (" + vat + " EUR PVN)";
                  pricePreview.innerText = priceWithVat.toFixed(2) + " EUR (" + vat + " EUR PVN)";
              }
              else{
                  vat = 0.00;
                  price.innerText = (totalPrice).toFixed(2) + " EUR (" + 0.00 + " EUR PVN)";
                  pricePreview.innerText = (totalPrice ).toFixed(2) + " EUR (" + 0.00 + " EUR PVN)";
              }
          }
  
          function openNav() {
              document.getElementById("myNav").style.height = "100%";
          }
  
          function closeNav() {
              document.getElementById("myNav").style.height = "0%";
              clearForm();
              
          }
  
          function clearForm(){
              let form = document.getElementById('order-form').elements;
              for(i=0;i<form.length;i++){
                  if(form[i].type=="text" || form[i].type=="tel" || form[i].type=="email"){
                      form[i].value="";
                  }
              }
          }